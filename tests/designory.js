module.exports = {
    "Menu Verification": browser => {
        browser.url("https://www.designory.com/")
        browser.pause(1000)

        // A.) verify that the menu contains six options: Work, About, Careers, Locations, Contact, News

        browser.click("#nav-toggle > button:nth-child(1)")
        browser.pause(1000)

        const workText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(1)"
        browser.assert.textContains(workText, "WORK")
        

        const aboutText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(2)"
        browser.assert.textContains(aboutText, "ABOUT")
        

        const careersText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(3)"
        browser.assert.textContains(careersText, "CAREERS")
        

        const locationsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(4)"
        browser.assert.textContains(locationsText, "LOCATIONS")
        

        const contactText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(5)"
        browser.assert.textContains(contactText, "CONTACT")
        

        const newsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(6)"
        browser.assert.textContains(newsText, "NEWS")
        

        // B.) verify that all six options take the user to relevant pages and Verify that on those relevant pages menu options are the same.

        //Work
        browser.click(".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1)")
        browser.pause(1000)

        browser.url(function(result) {
                // return the current url
                console.log(result);
              });

        //About
        browser.click("#nav-toggle > button:nth-child(1)")
        browser.pause(1000)
         
        //Verify that on those relevant pages menu options are the same.
        const aWorkText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(1)"
        browser.assert.textContains(aWorkText, "WORK")
        

        const aAboutText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(2)"
        browser.assert.textContains(aAboutText, "ABOUT")
        

        const aCareersText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(3)"
        browser.assert.textContains(aCareersText, "CAREERS")
        

        const aLocationsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(4)"
        browser.assert.textContains(aLocationsText, "LOCATIONS")
        

        const aContactText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(5)"
        browser.assert.textContains(aContactText, "CONTACT")
        

        const aNewsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(6)"
        browser.assert.textContains(aNewsText, "NEWS")
        

        browser.click(".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(2) > a:nth-child(1)")
        browser.pause(1000)

        browser.url(function(result) {
                // return the current url
                console.log(result);
              });

        //Careers
        browser.click(".navicon")
        browser.pause(1000)

         //Verify that on those relevant pages menu options are the same.
        const cWorkText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(1)"
        browser.assert.textContains(cWorkText, "WORK")
        

        const cAboutText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(2)"
        browser.assert.textContains(cAboutText, "ABOUT")
        

        const cCareersText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(3)"
        browser.assert.textContains(cCareersText, "CAREERS")
        

        const cLocationsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(4)"
        browser.assert.textContains(cLocationsText, "LOCATIONS")
        

        const cContactText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(5)"
        browser.assert.textContains(cContactText, "CONTACT")
        

        const cNewsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(6)"
        browser.assert.textContains(cNewsText, "NEWS")

        browser.click(".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(3) > a:nth-child(1)")
        browser.pause(1000)

        browser.url(function(result) {
                // return the current url
                console.log(result);
              });

        //Locations
        browser.click(".navicon")
        browser.pause(1000)
     
        //Verify that on those relevant pages menu options are the same.
        const lWorkText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(1)"
        browser.assert.textContains(lWorkText, "WORK")

        const lAboutText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(2)"
        browser.assert.textContains(lAboutText, "ABOUT")
        

        const lCareersText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(3)"
        browser.assert.textContains(lCareersText, "CAREERS")
        

        const lLocationsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(4)"
        browser.assert.textContains(lLocationsText, "LOCATIONS")
        

        const lContactText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(5)"
        browser.assert.textContains(lContactText, "CONTACT")
        

        const lNewsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(6)"
        browser.assert.textContains(lNewsText, "NEWS")

        browser.click(".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(4)")
        browser.pause(1000)

        browser.click(".subnav > li:nth-child(1) > a:nth-child(1)")
        browser.pause(1000)

        browser.url(function(result) {
                // return the current url
                console.log(result);
              });

         //Contact
         browser.click(".navicon")
         browser.pause(1000)
 
         //Verify that on those relevant pages menu options are the same.
         const crWorkText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(1)"
         browser.assert.textContains(crWorkText, "WORK")
 
         const crAboutText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(2)"
         browser.assert.textContains(crAboutText, "ABOUT")
         
 
         const crCareersText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(3)"
         browser.assert.textContains(crCareersText, "CAREERS")
         
 
         const crLocationsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(4)"
         browser.assert.textContains(crLocationsText, "LOCATIONS")
         
 
         const crContactText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(5)"
         browser.assert.textContains(crContactText, "CONTACT")
         
 
         const crNewsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(6)"
         browser.assert.textContains(crNewsText, "NEWS")

         browser.click(".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(5)")
         browser.pause(1000)
 
         browser.url(function(result) {
                // return the current url
                console.log(result);
              });

         //News
         browser.click(".navicon")
         browser.pause(1000)

         //Verify that on those relevant pages menu options are the same.
         const nWorkText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(1)"
         browser.assert.textContains(nWorkText, "WORK")
 
         const nAboutText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(2)"
         browser.assert.textContains(nAboutText, "ABOUT")
         
 
         const nCareersText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(3)"
         browser.assert.textContains(nCareersText, "CAREERS")
         
 
         const nLocationsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(4)"
         browser.assert.textContains(nLocationsText, "LOCATIONS")
         
 
         const nContactText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(5)"
         browser.assert.textContains(nContactText, "CONTACT")
         
 
         const nNewsText = ".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(6)"
         browser.assert.textContains(nNewsText, "NEWS")
 
         browser.click(".nav-wrapper > nav:nth-child(1) > ul:nth-child(1) > li:nth-child(6)")
         browser.pause(1000)
 
         browser.url(function(result) {
                // return the current url
                console.log(result);
              });

              browser.end()
        },

        "Cookie Verification": browser => {
                browser.url("https://www.designory.com/")
                browser.pause(1000)

        // 2 A.) Verify that after accepting the cookie notice, the user doesn't see the notice anymore.
        browser.click("html body.dark div#CybotCookiebotDialog div#CybotCookiebotDialogBody div#CybotCookiebotDialogBodyButtons a#CybotCookiebotDialogBodyButtonAccept.CybotCookiebotDialogBodyButton")
        browser.pause(1000)

        
        browser.assert.not.visible({selector: 'html body.dark div#CybotCookiebotDialog div#CybotCookiebotDialogBody div#CybotCookiebotDialogBodyButtons a#CybotCookiebotDialogBodyButtonAccept.CybotCookiebotDialogBodyButton', supressNotFoundErrors: true});

        // C.) Verify that after clearing cookies the “cookie notice” shows up again. 
        browser.deleteCookies()

        browser.refresh() 

        browser.assert.visible({selector: 'html body.dark div#CybotCookiebotDialog div#CybotCookiebotDialogBody div#CybotCookiebotDialogBodyButtons a#CybotCookiebotDialogBodyButtonAccept.CybotCookiebotDialogBodyButton', supressNotFoundErrors: true});

        // 3.) Verify that the Chicago location is present and that the following information is correctly displayed: 
        browser.end()
        },

        "Verify that the Chicago location": browser => {
                browser.url("https://www.designory.com/locations/chicago")
                browser.pause(1000)
        // a H1 is "CHI"
        browser.expect.element('h1').text.to.equal('CHI');  

        // b Phone number  is "Phone: +1 312 729 4500"
        const phoneText = ".grid-md-5 > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > p:nth-child(2)"
        browser.assert.textContains(phoneText, "Phone: +1 312 729 4500")

                //H2 font size is "40px"
                browser.getElementSize("h2", function(result) {
                        // return the current url
                        console.log(result);
                      });


                browser.end()
        },

        "Verify Map URL": browser => {
                browser.url("http://maps.google.com/?q=%20225%20N%20Michigan%20Ave,%20Suite%202100%20Chicago,%20IL%2060601")
                browser.pause(1000)

        browser.url(function(result) {
                // return the current url
                console.log(result);
              });

    }
}